//
//  API.swift
//  MusicOnline
//
//  Created by anh.buiphuong on 8/22/18.
//  Copyright © 2018 anh.buiphuong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class API{
    var mangager: SessionManager!
    var endpoint : String = ""
    var headersCommon: [String: String] = [:] // trong truong hop yeu cau can 1 so thong tin trong header, trong app nay k can
    static var path: String = "" // trong du an co path cua url thay doi, trong app nay k can
    static var timeout: Double = 60
    let method: HTTPMethod = .get
    public enum endpointType: String {
        case search = "search"
        func method() -> HTTPMethod { // trong nhieu du an k chi co moi search, va phuong thuc get du lieu
            switch self {
            case .search:
                return .get
            }
        }
    }
    init(endpoint: String, method: HTTPMethod = .get){
        self.endpoint = endpoint 
        self.createSessionManager()
    }
    init() {
        self.createSessionManager()
    }
    private func createSessionManager(){
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = TimeInterval(API.timeout)
       self.mangager = SessionManager(configuration: configuration)
    }
    public func call(parameters: [String: Any], isYoutubeApi: Bool = false, headerAdditional: [String: String]? = nil, fail: @escaping (_ error: JSON) -> (), success: @escaping (_ data : JSON) -> ()) {
        self.networkOperation(method: self.method, parameters: parameters, fail: fail, success: success)
    }
    private func networkOperation(method useMethod: HTTPMethod, parameters: [String:Any], isYoutubeApi: Bool = false, headerAdditional: [String: String]? = nil, fail: @escaping (_ error: JSON) -> (), success: @escaping (_ data : JSON) -> ()){
        var header = headersCommon
        if headerAdditional != nil {
            for (k,v) in headerAdditional! {
                 header[k] = v
            }
        }
        // var url = API.path + endpoint 
         var url = endpoint
        if endpoint.hasPrefix("http") {
            url = endpoint
        }
        var params: [String:Any]?
        if useMethod == .get {
            if parameters.count > 0 {
                var paramsToAdd: [String:Any] = [:]
                for (k,v) in parameters {
                    paramsToAdd[k] = v
                }
                if paramsToAdd.count > 0 {
                    var api = ""
                    for (k,v) in paramsToAdd {
                        api = api + k + "=" + "\(v)" + "&" //  v la any chua phai string
                        
                    }
                    if api.hasSuffix("&"){
                        api = String(api.dropLast())
                    }
                    url = "\(url)?\(api)"
                }
                params = paramsToAdd
            } else {
                params = parameters
            }
        }
        print("URL: ", url)
    }
}
