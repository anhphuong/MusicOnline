//
//  HomeServices.swift
//  MusicOnline
//
//  Created by anh.buiphuong on 8/22/18.
//  Copyright © 2018 anh.buiphuong. All rights reserved.
//

import UIKit
import SwiftyJSON
class API1 {
    static func getAPI(key: String, part: String, maxResults: String, type: String, q: String, order: String) -> String{
        let api = "https://www.googleapis.com/youtube/v3/search?key=\(key)&part=\(part)&maxResults=\(maxResults)&type=\(type)&q=\(q)&order=\(order)"
        return api
    }
    var api : API!
    func getPlaylists(success: (([Playlist]) -> ())?, fail: (() -> ())?) {
        let key = "AIzaSyBsNenpUZWN5y6RmcoAQ1Uv9vFtddV1Lg0"
        let part = "snippet"
        let maxResults = "50"
        let type = "playlist"
        let q = "Music"
        let order = "viewCount"
        let urlString = API1.getAPI(key: key, part: part, maxResults: maxResults, type: type, q: q, order: order)
        if let url = URL(string: urlString){
            if let data = try? String(contentsOf: url){
                let json = JSON(parseJSON: data)
                if let items = json["items"].array{
                    let playLists = items.map({ Playlist(items: $0)})
                    DispatchQueue.main.async {
                        success?(playLists)
                    }
                } else {
                    DispatchQueue.main.async {
                        fail?()
                    }
                }
                
            } else {
                DispatchQueue.main.async {
                    fail?()
                }
            }
        } else {
            DispatchQueue.main.async {
                fail?()
            }
        }
    }
    
    func getBannerVideos(success: (([Video]) -> ())? , fail: (() -> ())?) {
        let params: [String: Any] = ["key": "AIzaSyBsNenpUZWN5y6RmcoAQ1Uv9vFtddV1Lg0",
                                     "part": "snippet",
                                     "maxResults": 10,
                                     "type": "video",
                                     "q": "Music",
                                     "order": "viewCount",
                                     ]
        api = API(endpoint: "https://www.googleapis.com/youtube/v3/search")
        api.call(parameters: params, fail: { (_) in
            fail?()
        }) { (json) in
            if let items = json["items"].array {
                let videos = items.map({ Video(json: $0) })
                success?(videos)
            } else {
                fail?()
            }
        }
        
        
    }
    
    
    
    func getVideo(success: (([Video]) -> ())?, fail: (() -> ())?) {
        let key = "AIzaSyBsNenpUZWN5y6RmcoAQ1Uv9vFtddV1Lg0"
        let part = "snippet"
        let maxResults = "10"
        let type = "video"
        let q = "Music"
        let order = "viewCount"
        let urlString = API1.getAPI(key: key, part: part, maxResults: maxResults, type: type, q: q, order: order)
        if let url = URL(string: urlString){
            if let data = try? String(contentsOf: url){
                let json = JSON(parseJSON: data)
                if let items = json["items"].array{
                    let videos = items.map({ Video(json: $0)})
                    DispatchQueue.main.async {
                        success?(videos)
                    }
                } else {
                    DispatchQueue.main.async {
                        fail?()
                    }
                }
                
            } else {
                DispatchQueue.main.async {
                    fail?()
                }
            }
        } else {
            DispatchQueue.main.async {
                fail?()
            }
        }
    }
}
