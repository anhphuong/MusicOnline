//
//  HomeBannerView.swift
//  MusicOnline
//
//  Created by anh.buiphuong on 8/17/18.
//  Copyright © 2018 anh.buiphuong. All rights reserved.
//

import UIKit

class HomeBannerView: UIView, UIScrollViewDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var centerImageView: UIImageView!
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var rightImageView: UIImageView!
    private var videos: [Video] = []
    private var homeServices =  API1()
    private var timer: Timer!
    var currentIndex = 0 {
        didSet {
            if videos.count != 0 {
                centerImageView.sd_setImage(with: URL(string: getCenterImage()!), completed: nil)
                leftImageView.sd_setImage(with: URL(string: getPreviousImage()!), completed: nil)
                rightImageView.sd_setImage(with: URL(string: getNextImage()!), completed: nil)
                scrollView.setContentOffset(CGPoint(x: scrollView.frame.width, y: 0), animated: false)
                pageControl.currentPage = currentIndex

            }
        }
    }
    
    func setupView() {
        scrollView.isScrollEnabled = false
        scrollView.delegate = self
        scrollView.setContentOffset(CGPoint(x: scrollView.frame.width, y: 0), animated: false)
        timer = Timer.scheduledTimer(timeInterval: 3.5 , target: self, selector: #selector(autoNextImage), userInfo: nil , repeats: true)
        setupThumbnailsForHome()
    }
    
    @objc func autoNextImage() {
        scrollView.setContentOffset(CGPoint(x: scrollView.frame.width * 2, y: 0), animated: true)
   //     currentIndex = updateIndexNextImage()
        
    }
    func setupThumbnailsForHome(){
        scrollView.isScrollEnabled = true
        pageControl.numberOfPages = videos.count
       //  self.videos = videos
        homeServices.getBannerVideos(success:{ [unowned self] (videos) in
            self.videos = videos 
        }) {
            
        }
        currentIndex = 0
        pageControl.currentPage = 0
    }
    
    func getCenterImage() -> String? {
        if videos.isEmpty {
            return nil
        }
        return videos[currentIndex].thumbnailsURL
    }
    
    func getPreviousImage() -> String? {
        if videos.isEmpty {
            return nil
        }
        
        return videos[updateIndexPrevImage()].thumbnailsURL
    }
    
    func getNextImage() -> String? {
        if videos.isEmpty {
            return nil
        }
        
        return videos[updateIndexNextImage()].thumbnailsURL
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x == 0 {
            currentIndex = updateIndexPrevImage()
        }
        if scrollView.contentOffset.x == scrollView.frame.width * 2 {
            currentIndex = updateIndexNextImage()
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        timer.invalidate()
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        timer = Timer.scheduledTimer(timeInterval: 3.5 , target: self, selector: #selector(autoNextImage), userInfo: nil , repeats: true)
    }
    
    func updateIndexNextImage() -> Int {
        var nextIndex = currentIndex + 1
        if nextIndex == videos.count {
            nextIndex = 0
        }
        return nextIndex
    }
    
    func updateIndexPrevImage() -> Int {
        var previousIndex = currentIndex - 1
        if previousIndex < 0 {
            previousIndex = videos.count - 1
        }
        return previousIndex
    }
    
}
