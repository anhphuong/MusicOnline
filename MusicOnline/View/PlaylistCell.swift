//
//  PlaylistCell.swift
//  MusicOnline
//
//  Created by kien on 13/08/2018.
//  Copyright © 2018 anh.buiphuong. All rights reserved.
//

import UIKit
let playlistCell = "PlaylistCell"
class PlaylistCell: UICollectionViewCell {
    @IBOutlet weak var thumbnails: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var channelTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func update(playlist: Playlist) {
        thumbnails.sd_setImage(with: URL(string: playlist.thumbnails!), completed: nil)
        channelTitle.text = playlist.channelTitle
        title.text = playlist.title
    }

}
