//
//  HomeTabView.swift
//  MusicOnline
//
//  Created by anh.buiphuong on 8/17/18.
//  Copyright © 2018 anh.buiphuong. All rights reserved.
//

import UIKit

enum TabViewType: Int {
    case home = 0
    case playlist
}

protocol HomeTabViewDelegate: class {
    func didSelected(type: TabViewType)
}

class HomeTabView: UIView {
    fileprivate var underlineView: UIView!
    @IBOutlet private weak var homeButton: UIButton!
    @IBOutlet private weak var playlistButton: UIButton!

    weak var delegate: HomeTabViewDelegate?
    
    init() {
        super.init(frame: .zero)
        configView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configView()
    }
    
    func configView() {
        if let view = Bundle.main.loadNibNamed("HomeTabView", owner: self, options: nil)?.first as? UIView {
            self.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
            NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
            NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0).isActive = true
            NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 0).isActive = true
        }
        
    }
    
    func setButton(type: TabViewType) {
        switch type {
        case .home:
            onTabviewSelected(button: homeButton)
        case .playlist:
            onTabviewSelected(button: playlistButton)
        }
    }
    
    func setupUnderlineView() {
        var frame = homeButton.frame
        frame.origin.y += frame.height - 1
        frame.size.height = 1
        underlineView = UIView(frame: frame)
        underlineView.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        underlineView.backgroundColor = UIColor.purple
        self.addSubview(underlineView)
    }

    @IBAction func onTabviewSelected(button: UIButton) {
        var frame = underlineView.frame
        frame.origin.x = button.frame.origin.x
        UIView.animate(withDuration: 0.3) {
            self.underlineView.frame = frame
        }
        if button == homeButton {
            delegate?.didSelected(type: .home)
        } else {
            delegate?.didSelected(type: .playlist)
        }
        
    }
}
