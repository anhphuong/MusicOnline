//
//  NewMusicView.swift
//  MusicOnline
//
//  Created by anh.buiphuong on 8/17/18.
//  Copyright © 2018 anh.buiphuong. All rights reserved.
//

import UIKit

class NewMusicView: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var collectionViewForMusic: UICollectionView!
    var playlists: [Playlist] = []
    func setUpView() {
        collectionViewForMusic.dataSource = self
        collectionViewForMusic.delegate = self
        collectionViewForMusic.register(UINib(nibName: playlistCell, bundle: Bundle.main), forCellWithReuseIdentifier: playlistCell)
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playlists.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: playlistCell, for: indexPath) as! PlaylistCell
        cell.update(playlist: playlists[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionViewForMusic.frame.width - 20) / 3  , height: collectionViewForMusic.frame.height)
    }
    
    
}
