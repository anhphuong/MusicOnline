//
//  TrendingView.swift
//  MusicOnline
//
//  Created by anh.buiphuong on 8/17/18.
//  Copyright © 2018 anh.buiphuong. All rights reserved.
//

import UIKit

class TrendingView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var collectionViewForTrend: UICollectionView!
    
    func setUpView() {
        collectionViewForTrend.dataSource = self
        collectionViewForTrend.delegate = self
    //    collectionViewForTrend.register(UINib(nibName: trendingCell, bundle: Bundle.main), forCellWithReuseIdentifier: trendingCell)
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell" , for: indexPath)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionViewForTrend.frame.size.width - 12) / 4, height: (collectionViewForTrend.frame.height - 3) / 2)
    }
    
}
