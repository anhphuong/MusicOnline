//
//  HomeViewController.swift
//  MusicOnline
//
//  Created by anh.buiphuong on 8/15/18.
//  Copyright © 2018 anh.buiphuong. All rights reserved.
//

import UIKit

class HomeViewController: UITableViewController{
    
    @IBOutlet weak var homeBannerView: HomeBannerView!
    @IBOutlet weak var newMusicView: NewMusicView!
    @IBOutlet weak var trendingView: TrendingView!
    lazy var apiService = API1()
    var videos: [Video] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        homeBannerView.setupView()
//        apiService.getVideo(success: { [weak self](videos) in
//            self?.videos = videos
//            self?.tableView.reloadData()
//            self?.homeBannerView.setupThumbnailsForHome()
//        }) {
//
//        }
        newMusicView.setUpView()
        apiService.getPlaylists(success: { [weak self] (playlist) in
            self?.newMusicView.playlists = playlist
            self?.newMusicView.collectionViewForMusic.reloadData()
        }) {
            
        }
        trendingView.setUpView()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
