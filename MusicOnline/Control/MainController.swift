//
//  ViewController.swift
//  MusicOnline
//
//  Created by anh.buiphuong on 8/13/18.
//  Copyright © 2018 anh.buiphuong. All rights reserved.
//

import UIKit
import SwiftyJSON
class MainController: UIViewController, UIScrollViewDelegate, HomeTabViewDelegate {
    
    

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tabview: HomeTabView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tabview.delegate = self 
        scrollView.delegate = self
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         tabview.setupUnderlineView()
        
    }
    
    
    
    func didSelected(type: TabViewType) {
        switch type {
        case .home:
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        case .playlist:
            scrollView.setContentOffset(CGPoint(x: view.frame.width, y: 0), animated: true)
        }
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let x = targetContentOffset.pointee.x
        if x >= view.frame.width {
            tabview.setButton(type: .playlist)
        } else {
            tabview.setButton(type: .home)
        }
    }

    
}

