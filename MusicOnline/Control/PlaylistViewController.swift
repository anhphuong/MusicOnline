//
//  PlaylistViewController.swift
//  MusicOnline
//
//  Created by kien on 13/08/2018.
//  Copyright © 2018 anh.buiphuong. All rights reserved.
//

import UIKit

class PlaylistViewController: UIViewController{

    @IBOutlet weak var collectionView: UICollectionView!
    fileprivate var playLists: [Playlist] = []
    fileprivate var collectionItemSize: CGSize?
    fileprivate var isLoadingMore: Bool = false
    lazy var apiService = API1()
    
    func getCollectionItemSize() -> CGSize {
        if collectionItemSize == nil {
            let x = (view.frame.size.width - 5) / 3
            collectionItemSize = CGSize(width: x, height: 1.5 * x)
        }
        return collectionItemSize!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: playlistCell, bundle: Bundle.main), forCellWithReuseIdentifier: playlistCell)
        apiService = API1()
        apiService.getPlaylists(success: { [weak self] (playlist) in
            self?.playLists = playlist
            self?.collectionView.reloadData()
        }) {
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
}

extension PlaylistViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: playlistCell, for: indexPath) as! PlaylistCell
        cell.update(playlist: playLists[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playLists.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return getCollectionItemSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.height && !isLoadingMore {
            isLoadingMore = true 
        }
    }
    
}
