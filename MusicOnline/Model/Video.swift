//
//  Video.swift
//  MusicOnline
//
//  Created by anh.buiphuong on 8/15/18.
//  Copyright © 2018 anh.buiphuong. All rights reserved.
//

import Foundation
import SwiftyJSON
class Video {
    var thumbnailsURL: String?
    var thumbnailsImage: UIImage?
    init(json: JSON) {
        if let bannerURL = json["snippet"]["thumbnails"]["high"]["url"].string{
            thumbnailsURL = bannerURL
            let url = URL(string: bannerURL)
            let data = try? Data(contentsOf: url!)
            thumbnailsImage = UIImage(data: data!)
        }
    }
}
