//
//  Playlist.swift
//  MusicOnline
//
//  Created by anh.buiphuong on 8/14/18.
//  Copyright © 2018 anh.buiphuong. All rights reserved.
//

import UIKit
import SwiftyJSON
class Playlist{
    var thumbnails: String?
    var title: String?
    var channelTitle: String?
    var id: String?
    init(items: JSON)  {
        if let thumb = items["snippet"]["thumbnails"]["high"]["url"].string {
            self.thumbnails = thumb
        }
        if let tit = items["snippet"]["title"].string{
            self.title = tit
        }
        if let channel = items["snippet"]["channelTitle"].string{
            self.channelTitle = channel
        }
        if let id = items["id"]["playlistId"].string {
            self.id = id
        }
    }
}
